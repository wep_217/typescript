interface Rectangle1 {
    height1: number,
    width1: number
  }
  
  interface ColoredRectangle1 extends Rectangle1 {
    color: string
  }

  const rectangle1: Rectangle1 = {
    height1: 20,
    width1: 10
  };

  console.log(rectangle1);

  const coloredRectangle1: ColoredRectangle1 = {
    height1: 20,
    width1: 10,
    color: "red"
  };

  console.log(coloredRectangle1);